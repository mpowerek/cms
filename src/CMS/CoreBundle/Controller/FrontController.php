<?php

namespace App\CMS\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    public function index()
    {
        return $this->render('@CMSCoreBundle/frontPage.html.twig');
    }
}
